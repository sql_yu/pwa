
//service-worker
import { Sw, register, askPermission, installprompt, forceUpdate, syncData, subscribeToPush, postMessage } from './register-service-worker.js'

/**
 * @param {*} swUrl | '/service-worker.js'  service-worker url
 * @param {*} registrationOptions  |{ scope: '/' }
 */
export default function (swUrl, registrationOptions = { scope: '/' }) {
    register(swUrl || './service-worker.js', {
        ready(data) {//可以使用pwa 功能
            log('ready')
        },
        parsed(data) {
            log('parsed')
        },
        install(data) {
            log('install')
        },
        installing(data) {
            log('installing')
        },
        installed(data) {
            log('installed')
        },
        beforeUpdate(data) {
            log('beforeUpdate')
            notifyInfo()
        },
        activating(data) {
            log('activating')
        },
        activate(data) {
            log('activate')
        },
        activated(data) {
            log('activated')
        },
        updatedCache(data) {
            log('updatedCache')
        },
        updated(data) {
            log('updated')
        },
        fetch(data) {
            log('fetch')
        },
        sync(data) {
            log('sync')
        },
        online(data) {
            log('online')
        },
        offline(data) {
            log('offline')
        },
        error(error) {
            console.error('Error', error)
        },
        ...registrationOptions
    })
}

function log(text) {
    document.getElementById('log').innerText += text + ' => ';
}
/**
 * 通知用户
 */
function notifyInfo(registration) {
    return Promise.all([registration || navigator.serviceWorker.getRegistration(), askPermission()]).then((results) => {
        const registration = results[0];
        registration.showNotification("PWA TEST", {
            body: '版本有更新资源缓存',
            data: '携带的数据',
            icon: './tobog.png',
            badge: './tobog.png',
            tag: 'update',
            timestamp: Date.now(),
            actions: [{
                action: 'update',
                title: `更新缓存`,
                icon: './tobog.png',
            }, {
                action: 'cancel',
                title: '取消更新',
                icon: './tobog.png',
            }],
            renotify: true,
            requireInteraction: true,
        });
    }).catch(err => {
        if (Notification.permission === 'denied' && window.confirm(`是否强制更新${'VUE_APP_VERSION'}版本`)) {
            forceUpdate()
        }
    })
}


document.getElementById('add-home').onclick = installprompt()
document.getElementById('notify').onclick = () => notifyInfo()
document.getElementById('sync').onclick = () => syncData()
document.getElementById('update').onclick = () => Sw.update()
document.getElementById('updateCache').onclick = () => postMessage('updateCache')
document.getElementById('push').onclick = () => subscribeToPush('/push', 'BOEQSjdhorIf8M0XFNlwohK3sTzO9iJwvbYU-fuXRF0tvRpPPMGO6d_gJC_pUQwBT7wD8rKutpNTFHOHN3VqJ0A')